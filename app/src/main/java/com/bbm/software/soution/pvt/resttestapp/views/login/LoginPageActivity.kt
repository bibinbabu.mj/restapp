package com.bbm.software.soution.pvt.resttestapp.views.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bbm.software.soution.pvt.resttestapp.R
import com.bbm.software.soution.pvt.resttestapp.databinding.ActivityLoginPageBinding
import com.bbm.software.soution.pvt.resttestapp.network.RetrofitProvider
import com.bbm.software.soution.pvt.resttestapp.repo.AppMainRepo
import com.bbm.software.soution.pvt.resttestapp.sealed.LoginResultApiCallBacks
import com.bbm.software.soution.pvt.resttestapp.utils.NetworkConnectionChecker
import com.bbm.software.soution.pvt.resttestapp.utils.mSnackBar
import com.bbm.software.soution.pvt.resttestapp.utils.mToast

class LoginPageActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mBinding: ActivityLoginPageBinding
    lateinit var mAppMainRepo: AppMainRepo
    lateinit var mLoginViewModel: LoginPageViewModel
    lateinit var mLoginViewModelFactory: LoginViewModelFactory
    private var isFirstTime = true
    private lateinit var mNetworkConnectionChecker: NetworkConnectionChecker
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginPageBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mInit()
        mInitControls()
        mObserveResult()
    }

    private fun mInitControls() {
        checkNetworkConnection()
        mBinding.submit.setOnClickListener(this)
    }

    private fun mInit() {
        mNetworkConnectionChecker = NetworkConnectionChecker(this)
        mAppMainRepo = AppMainRepo(RetrofitProvider.invoke())
        mLoginViewModelFactory = LoginViewModelFactory(mAppMainRepo)
        mLoginViewModel =
            ViewModelProvider(this, mLoginViewModelFactory).get(LoginPageViewModel::class.java)
    }


    private fun checkNetworkConnection() {
        mNetworkConnectionChecker.observe(this) { isConnected ->
            if (isConnected) {
                mSnackBar(mBinding.continer, "Back to online", "#1eb2a6")
            } else {
                mSnackBar(mBinding.continer, "No network", "#FFE80C0C")

            }
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            mBinding.submit -> {
                if (mCheckIsEmptyorNot()) {
                    mBinding.progressBar.visibility = View.VISIBLE
                    mLoginViewModel.mUserLoginData(
                        mBinding.username.text.toString(),
                        mBinding.userPin.text.toString()
                    )
                } else {
                    mToast("Empty Fields please enter details")
                }
            }
        }

    }

    private fun mCheckIsEmptyorNot(): Boolean {
        return !(mBinding.username.text.isNullOrEmpty() || mBinding.userPin.text.isNullOrEmpty())
    }

    private fun mObserveResult() {
        mLoginViewModel.mLoginResultCallBacks.observe(this) {

            when (it) {
                is LoginResultApiCallBacks.OnFail -> {
                    mBinding.progressBar.visibility = View.GONE
                    mBinding.accountNumberTxt.visibility = View.INVISIBLE
                    mToast(it.mMeassage)
                   // isFirstTime = false
                   // checkNetworkConnection()
                }
                is LoginResultApiCallBacks.OnLoading -> {
                    mBinding.accountNumberTxt.visibility = View.INVISIBLE

                }
                is LoginResultApiCallBacks.OnSucess -> {
                    mBinding.progressBar.visibility = View.GONE
                    if (it.mLoginResult.mem_pin == "-1") {
                        mBinding.accountNumberTxt.visibility = View.INVISIBLE
                        mToast("Account not found")
                    } else {
                        mBinding.accountNumberTxt.visibility = View.VISIBLE
                        mBinding.accountNumberTxt.text = it.mLoginResult.mem_account
                    }

                }
            }
        }
    }
}