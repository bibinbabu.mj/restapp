package com.bbm.software.soution.pvt.resttestapp.views.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bbm.software.soution.pvt.resttestapp.repo.AppMainRepo

class LoginViewModelFactory(var mRepo: AppMainRepo) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LoginPageViewModel(mRepo) as T
    }
}