package com.bbm.software.soution.pvt.resttestapp.model

data class LoginResultModel(
    var mem_username: String,
    var mem_pin: String,
    var mem_account: String
)
