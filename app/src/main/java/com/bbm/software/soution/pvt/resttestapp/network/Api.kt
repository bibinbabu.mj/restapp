package com.bbm.software.soution.pvt.resttestapp.network

import com.bbm.software.soution.pvt.resttestapp.model.LoginResultModel
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    @GET("getaccount/{mem_username}/{mem_pin}")
    suspend fun mGetUserDetails(
        @Path(value = "mem_username") username: String,
        @Path(value = "mem_pin") pin: String
    ): Response<LoginResultModel>
}