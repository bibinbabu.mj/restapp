package com.bbm.software.soution.pvt.resttestapp.views.login


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bbm.software.soution.pvt.resttestapp.coroutine.Coroutines
import com.bbm.software.soution.pvt.resttestapp.model.LoginResultModel
import com.bbm.software.soution.pvt.resttestapp.repo.AppMainRepo
import com.bbm.software.soution.pvt.resttestapp.sealed.LoginResultApiCallBacks
import com.bbm.software.soution.pvt.resttestapp.utils.FailNetWorkCallBack
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel

class LoginPageViewModel(val mRepo: AppMainRepo) : ViewModel(), FailNetWorkCallBack {
    lateinit var mJob: Job
    private val mUserLoginResult = MutableLiveData<LoginResultModel>()
   // val mLoginResult: LiveData<LoginResultModel> get() = mUserLoginResult
   private var mFailNetWorkCallBack: FailNetWorkCallBack =this
    ////////

    ///////
    private val mUserLoginResultCallBacks = MutableLiveData<LoginResultApiCallBacks>()
    val mLoginResultCallBacks: LiveData<LoginResultApiCallBacks> get() = mUserLoginResultCallBacks

    fun mUserLoginData(mUsername: String, mPin: String) {
        mUserLoginResultCallBacks.value = LoginResultApiCallBacks.OnLoading("OnLoading")
        mJob = Coroutines.mIOThenMain({mFailNetWorkCallBack},{ mRepo.mGetUserLogin(mUsername, mPin) }, {
            if (it!!.isSuccessful) {
                mUserLoginResultCallBacks.value = LoginResultApiCallBacks.OnSucess(it.body()!!)
                //  mUserLoginResult.value = it.body()
            } else {
                mUserLoginResultCallBacks.value = LoginResultApiCallBacks.OnFail("On Fail")
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        if (::mJob.isInitialized) mJob.cancel()
    }

    override fun onFail(mMessage: String) {
        mUserLoginResultCallBacks.value = LoginResultApiCallBacks.OnFail("On Network Fail")
    }

}