package com.bbm.software.soution.pvt.resttestapp.coroutine

import com.bbm.software.soution.pvt.resttestapp.utils.FailNetWorkCallBack
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

object Coroutines {
    fun <T : Any> mIOThenMain(
        mNetWorkCallBackListner: () -> FailNetWorkCallBack,
        work: suspend (() -> T?),
        callback: suspend ((T?) -> Unit)
    ) =
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val data = CoroutineScope(Dispatchers.IO).async rt@{
                    return@rt work()
                }.await()
                callback(data)
            } catch (e: Exception) {
                val listner = mNetWorkCallBackListner.invoke()
                listner.onFail(e.message.toString())
            }
        }


}