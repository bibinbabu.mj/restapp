package com.bbm.software.soution.pvt.resttestapp.sealed

import com.bbm.software.soution.pvt.resttestapp.model.LoginResultModel

sealed class LoginResultApiCallBacks {
    data class OnSucess(var mLoginResult: LoginResultModel):LoginResultApiCallBacks()
    data class OnFail(var mMeassage: String):LoginResultApiCallBacks()
    data class OnLoading(var mMeassage: String):LoginResultApiCallBacks()
}