package com.bbm.software.soution.pvt.resttestapp.repo

import com.bbm.software.soution.pvt.resttestapp.network.Api

class AppMainRepo(var api: Api) {
    suspend fun mGetUserLogin(mUsername:String,mPin:String) = api.mGetUserDetails(mUsername,mPin)
}