package com.bbm.software.soution.pvt.resttestapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkRequest
import androidx.lifecycle.LiveData

class NetworkConnectionChecker(mContext: Context) : LiveData<Boolean>() {
    private var mConnectivityManager: ConnectivityManager

    init {
        mConnectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    private val mNetworkCallBack = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            postValue(true)
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            postValue(false)
        }

        override fun onUnavailable() {
            super.onUnavailable()
            postValue(false)
        }

    }

    override fun onActive() {
        super.onActive()
        val mBuilder=NetworkRequest.Builder()
        mConnectivityManager.registerNetworkCallback(mBuilder.build(),mNetworkCallBack)
       // mConnectivityManager.registerNetworkCallback(mBuilder.addCapability(NET_CAPABILITY_INTERNET).build(),mNetworkCallBack)
    }

    override fun onInactive() {
        super.onInactive()
        mConnectivityManager.unregisterNetworkCallback(mNetworkCallBack)
    }

}