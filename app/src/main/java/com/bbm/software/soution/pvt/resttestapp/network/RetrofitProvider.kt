package com.bbm.software.soution.pvt.resttestapp.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitProvider {
    private var BASE_URL = "http://resttest.wmids.uk/rest/"
    private lateinit var mRetrofit: Retrofit
    operator fun invoke(): Api {
        mRetrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return mRetrofit.create(Api::class.java)
    }
}