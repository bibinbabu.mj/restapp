package com.bbm.software.soution.pvt.resttestapp.utils

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

fun Context.mToast(mMessage: String) {
    Toast.makeText(this, mMessage, Toast.LENGTH_SHORT).show()

}

fun mSnackBar(mView: View, mMessage: String, mColor: String) {
    val mSnackBar = Snackbar.make(mView, mMessage, Snackbar.LENGTH_LONG)
    mSnackBar.view.setBackgroundColor(Color.parseColor(mColor))
    mSnackBar.show()
}