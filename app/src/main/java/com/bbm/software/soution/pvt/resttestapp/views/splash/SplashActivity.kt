package com.bbm.software.soution.pvt.resttestapp.views.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import com.bbm.software.soution.pvt.resttestapp.R
import com.bbm.software.soution.pvt.resttestapp.databinding.ActivitySplashPageBinding
import com.bbm.software.soution.pvt.resttestapp.views.login.LoginPageActivity

class SplashActivity : AppCompatActivity() {
    private var mSplash_Timer = 3000
    lateinit var binding: ActivitySplashPageBinding

    private
    val mTimeCounter = object : CountDownTimer(mSplash_Timer.toLong(), 100) {
        override fun onTick(p0: Long) {

        }

        override fun onFinish() {
            nLoginPage()
        }

    }

    private fun nLoginPage() {
        val intent = Intent(this, LoginPageActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mTimeCounter.start()
    }
}